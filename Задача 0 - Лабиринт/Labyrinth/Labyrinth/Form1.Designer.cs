﻿namespace Labyrinth
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pb_Labyrinth = new System.Windows.Forms.PictureBox();
            this.but_Build_Lab = new System.Windows.Forms.Button();
            this.gb_Labyrinth = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gb_Building = new System.Windows.Forms.GroupBox();
            this.lab_Size = new System.Windows.Forms.Label();
            this.numUD_Size = new System.Windows.Forms.NumericUpDown();
            this.but_Solving = new System.Windows.Forms.Button();
            this.gb_Solving = new System.Windows.Forms.GroupBox();
            this.cb_Fixed_Dots = new System.Windows.Forms.CheckBox();
            this.gb_Saving = new System.Windows.Forms.GroupBox();
            this.but_Save_Labyrinth = new System.Windows.Forms.Button();
            this.cb_Arrow_Mode = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Labyrinth)).BeginInit();
            this.gb_Labyrinth.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gb_Building.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_Size)).BeginInit();
            this.gb_Solving.SuspendLayout();
            this.gb_Saving.SuspendLayout();
            this.SuspendLayout();
            // 
            // pb_Labyrinth
            // 
            this.pb_Labyrinth.Location = new System.Drawing.Point(16, 15);
            this.pb_Labyrinth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pb_Labyrinth.Name = "pb_Labyrinth";
            this.pb_Labyrinth.Size = new System.Drawing.Size(500, 500);
            this.pb_Labyrinth.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pb_Labyrinth.TabIndex = 0;
            this.pb_Labyrinth.TabStop = false;
            // 
            // but_Build_Lab
            // 
            this.but_Build_Lab.Location = new System.Drawing.Point(80, 30);
            this.but_Build_Lab.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.but_Build_Lab.Name = "but_Build_Lab";
            this.but_Build_Lab.Size = new System.Drawing.Size(200, 30);
            this.but_Build_Lab.TabIndex = 1;
            this.but_Build_Lab.Text = "Построить лабиринт";
            this.but_Build_Lab.UseVisualStyleBackColor = true;
            this.but_Build_Lab.Click += new System.EventHandler(this.But_Draw_Click);
            // 
            // gb_Labyrinth
            // 
            this.gb_Labyrinth.Controls.Add(this.panel1);
            this.gb_Labyrinth.Location = new System.Drawing.Point(381, 12);
            this.gb_Labyrinth.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gb_Labyrinth.Name = "gb_Labyrinth";
            this.gb_Labyrinth.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gb_Labyrinth.Size = new System.Drawing.Size(733, 677);
            this.gb_Labyrinth.TabIndex = 2;
            this.gb_Labyrinth.TabStop = false;
            this.gb_Labyrinth.Text = "Лабиринт:";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pb_Labyrinth);
            this.panel1.Location = new System.Drawing.Point(17, 16);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(699, 645);
            this.panel1.TabIndex = 0;
            // 
            // gb_Building
            // 
            this.gb_Building.Controls.Add(this.lab_Size);
            this.gb_Building.Controls.Add(this.numUD_Size);
            this.gb_Building.Controls.Add(this.but_Build_Lab);
            this.gb_Building.Location = new System.Drawing.Point(12, 12);
            this.gb_Building.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gb_Building.Name = "gb_Building";
            this.gb_Building.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gb_Building.Size = new System.Drawing.Size(363, 146);
            this.gb_Building.TabIndex = 3;
            this.gb_Building.TabStop = false;
            this.gb_Building.Text = "Построение:";
            // 
            // lab_Size
            // 
            this.lab_Size.AutoSize = true;
            this.lab_Size.Location = new System.Drawing.Point(80, 100);
            this.lab_Size.Name = "lab_Size";
            this.lab_Size.Size = new System.Drawing.Size(98, 17);
            this.lab_Size.TabIndex = 4;
            this.lab_Size.Text = "Размерность:";
            // 
            // numUD_Size
            // 
            this.numUD_Size.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUD_Size.Location = new System.Drawing.Point(209, 98);
            this.numUD_Size.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numUD_Size.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numUD_Size.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numUD_Size.Name = "numUD_Size";
            this.numUD_Size.Size = new System.Drawing.Size(71, 22);
            this.numUD_Size.TabIndex = 3;
            this.numUD_Size.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // but_Solving
            // 
            this.but_Solving.Enabled = false;
            this.but_Solving.Location = new System.Drawing.Point(80, 30);
            this.but_Solving.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.but_Solving.Name = "but_Solving";
            this.but_Solving.Size = new System.Drawing.Size(200, 30);
            this.but_Solving.TabIndex = 2;
            this.but_Solving.Text = "Пройти лабиринт";
            this.but_Solving.UseVisualStyleBackColor = true;
            this.but_Solving.Click += new System.EventHandler(this.But_Solve_Click);
            // 
            // gb_Solving
            // 
            this.gb_Solving.Controls.Add(this.cb_Arrow_Mode);
            this.gb_Solving.Controls.Add(this.cb_Fixed_Dots);
            this.gb_Solving.Controls.Add(this.but_Solving);
            this.gb_Solving.Location = new System.Drawing.Point(12, 164);
            this.gb_Solving.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gb_Solving.Name = "gb_Solving";
            this.gb_Solving.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gb_Solving.Size = new System.Drawing.Size(363, 162);
            this.gb_Solving.TabIndex = 4;
            this.gb_Solving.TabStop = false;
            this.gb_Solving.Text = "Решение:";
            // 
            // cb_Fixed_Dots
            // 
            this.cb_Fixed_Dots.AutoSize = true;
            this.cb_Fixed_Dots.Location = new System.Drawing.Point(80, 82);
            this.cb_Fixed_Dots.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_Fixed_Dots.Name = "cb_Fixed_Dots";
            this.cb_Fixed_Dots.Size = new System.Drawing.Size(175, 21);
            this.cb_Fixed_Dots.TabIndex = 3;
            this.cb_Fixed_Dots.Text = "Зафиксировать точки";
            this.cb_Fixed_Dots.UseVisualStyleBackColor = true;
            // 
            // gb_Saving
            // 
            this.gb_Saving.Controls.Add(this.but_Save_Labyrinth);
            this.gb_Saving.Location = new System.Drawing.Point(12, 334);
            this.gb_Saving.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gb_Saving.Name = "gb_Saving";
            this.gb_Saving.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gb_Saving.Size = new System.Drawing.Size(363, 176);
            this.gb_Saving.TabIndex = 1;
            this.gb_Saving.TabStop = false;
            this.gb_Saving.Text = "Сохранение результатов:";
            // 
            // but_Save_Labyrinth
            // 
            this.but_Save_Labyrinth.Location = new System.Drawing.Point(80, 75);
            this.but_Save_Labyrinth.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.but_Save_Labyrinth.Name = "but_Save_Labyrinth";
            this.but_Save_Labyrinth.Size = new System.Drawing.Size(200, 30);
            this.but_Save_Labyrinth.TabIndex = 0;
            this.but_Save_Labyrinth.Text = "Сохранить изображение";
            this.but_Save_Labyrinth.UseVisualStyleBackColor = true;
            this.but_Save_Labyrinth.Click += new System.EventHandler(this.But_Save_Labyrinth_Click);
            // 
            // cb_Arrow_Mode
            // 
            this.cb_Arrow_Mode.AutoSize = true;
            this.cb_Arrow_Mode.Location = new System.Drawing.Point(80, 118);
            this.cb_Arrow_Mode.Name = "cb_Arrow_Mode";
            this.cb_Arrow_Mode.Size = new System.Drawing.Size(158, 21);
            this.cb_Arrow_Mode.TabIndex = 1;
            this.cb_Arrow_Mode.Text = "Режим \"Стрелочки\"";
            this.cb_Arrow_Mode.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1617, 718);
            this.Controls.Add(this.gb_Saving);
            this.Controls.Add(this.gb_Solving);
            this.Controls.Add(this.gb_Building);
            this.Controls.Add(this.gb_Labyrinth);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Задача 0: Лабиринт";
            ((System.ComponentModel.ISupportInitialize)(this.pb_Labyrinth)).EndInit();
            this.gb_Labyrinth.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.gb_Building.ResumeLayout(false);
            this.gb_Building.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_Size)).EndInit();
            this.gb_Solving.ResumeLayout(false);
            this.gb_Solving.PerformLayout();
            this.gb_Saving.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_Labyrinth;
        private System.Windows.Forms.Button but_Build_Lab;
        private System.Windows.Forms.GroupBox gb_Labyrinth;
        private System.Windows.Forms.GroupBox gb_Building;
        private System.Windows.Forms.Button but_Solving;
        private System.Windows.Forms.Label lab_Size;
        private System.Windows.Forms.NumericUpDown numUD_Size;
        private System.Windows.Forms.GroupBox gb_Solving;
        private System.Windows.Forms.CheckBox cb_Fixed_Dots;
        private System.Windows.Forms.GroupBox gb_Saving;
        private System.Windows.Forms.Button but_Save_Labyrinth;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox cb_Arrow_Mode;
    }
}

