﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

using FunctionPlotter = FireGrid.FunctionPlotter;
using Labyrinths = LabyrinthNSP.Labyrinths;

namespace Labyrinth
{
    public partial class Form1 : Form
    {
        public FunctionPlotter plotter;
        public SmoothingMode mode = SmoothingMode.HighQuality;
        private Random random = new Random();
        static public int n = 10;
        public Labyrinths.Cell[,] labyrinth;
        public int[] start, end;
        public bool arrowMode = false;


        public void  Drawing()
        {
            plotter = new FunctionPlotter(pb_Labyrinth, 0, n + 2, 0, n + 2);
            plotter.SetGrades(n + 2, n + 2);
            plotter.Clear();
        }

        private void But_Draw_Click(object sender, EventArgs e)
        {
            n = Convert.ToInt16(numUD_Size.Value);
            if (n > 100)
                pb_Labyrinth.Size = new Size(n*3, n*3);
            but_Solving.Enabled = true;
            Drawing();
            Labyrinths.Cell[,] cells = new Labyrinths.Cell[n, n];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    cells[i, j] = new Labyrinths.Cell(true);
            labyrinth = Labyrinths.Get_Labyrinth(cells,random);
            start = Labyrinths.Get_Start_Cell(n, random);
            end = Labyrinths.Get_Start_Cell(n, random);
            plotter.DrawLabyrinth(labyrinth, false);
        }

        private void But_Solve_Click(object sender, EventArgs e)
        {
            Drawing();
            Labyrinths.Cell[,] solution = new Labyrinths.Cell[n,n];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    solution[i, j] = new Labyrinths.Cell(true);
            if(cb_Fixed_Dots.Checked)
                solution = Labyrinths.Solve_Labyrinth(labyrinth, random,ref start,ref end,false);
            else
                solution = Labyrinths.Solve_Labyrinth(labyrinth, random, ref start, ref end);
            plotter.DrawLabyrinth(solution, cb_Arrow_Mode.Checked);
        }

        public Form1()
        {
            InitializeComponent();

        }

        private void But_Save_Labyrinth_Click(object sender, EventArgs e)
        {
            if (pb_Labyrinth.Image != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла, отображаемый в поле "Тип файла"
                savedialog.Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pb_Labyrinth.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
