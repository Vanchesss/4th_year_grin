﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FunctionPlotter = FireGrid.FunctionPlotter;


namespace LabyrinthNSP
{
    public class Labyrinths
    {
        public FunctionPlotter plotter;

        public struct Cell
        {
            public bool up, down, left, right, visited;
            public int cellColor, direction;

            public Cell(bool withBoards)
            {
                if (withBoards == true)
                {
                    up = true;
                    down = true;
                    left = true;
                    right = true;
                    visited = false;
                    cellColor = 0;
                    direction = 0;
                }
                else
                {
                    up = false;
                    down = false;
                    left = false;
                    right = false;
                    visited = false;
                    cellColor = 0;
                    direction = 0;
                }
            }
        }

        public static int Get_Direction(Cell[,] cells, int row, int column, Random random, bool building = true)
        {
            int n = cells.GetLength(0);
            List<int> result = new List<int>();
            if (building)
            {
                if (row != 0 && cells[row - 1, column].visited == false)
                    result.Add(1);
                if (row != (n - 1) && cells[row + 1, column].visited == false)
                    result.Add(2);
                if (column != 0 && cells[row, column - 1].visited == false)
                    result.Add(3);
                if (column != (n - 1) && cells[row, column + 1].visited == false)
                    result.Add(4);
            }
            else
            {
                if (row != 0 && cells[row, column].down == false && cells[row - 1, column].visited == false)
                    result.Add(1);
                if ((row != (n - 1) && cells[row, column].up == false && cells[row + 1, column].visited == false))
                    result.Add(2);
                if ((column != 0 && cells[row, column].left == false && cells[row, column - 1].visited == false))
                    result.Add(3);
                if ((column != (n - 1) && cells[row, column].right == false && cells[row, column + 1].visited == false))
                    result.Add(4);
            }
            int[] res = result.ToArray();

            if (res.Length == 0) return 0;

            return res[random.Next(0, res.Length)];
        }


        public static int[] Get_Start_Cell(int n, Random random)
        {
            int[] res = new int[3];
            int rand = (random.Next(0, n));
            int side = (random.Next(1, 5));
            switch (side)
            {
                case 1:
                    res[0] = 0; res[1] = rand; break;
                case 2:
                    res[0] = n - 1; res[1] = rand; break;
                case 3:
                    res[0] = rand; res[1] = 0; break;
                case 4:
                    res[0] = rand; res[1] = n - 1; break;
            }
            res[2] = side;
            return res;
        }

        public static Cell[,] Get_Labyrinth(Cell[,] cells, Random random)
        {
            Cell[,] res = cells;
            Stack<int> cells_stack = new Stack<int>();
            int[] start = Get_Start_Cell(cells.GetLength(0), random);
            int new_direction = 1;
            int row = start[0]; int column = start[1];
            /*switch (start[2])
            {
                case 1: res[row, column].down = false; break;
                case 2: res[row, column].up = false; break;
                case 3: res[row, column].left = false; break;
                case 4: res[row, column].right = false; break;
            }*/
            while (true)
            {
                res[row, column].visited = true;
                new_direction = Get_Direction(res, row, column, random);
                if (new_direction == 0)
                {
                    while (new_direction == 0)
                    {
                        int backward = cells_stack.Pop();
                        switch (backward)
                        {
                            case 1: row++; break;
                            case 2: row--; break;
                            case 3: column++; break;
                            case 4: column--; break;
                        }
                        if (row == start[0] && column == start[1])
                            return res;
                        new_direction = Get_Direction(res, row, column, random);
                    }
                }
                switch (new_direction)
                {
                    case 1: cells_stack.Push(1); res[row, column].down = false; row--;  res[row, column].up = false; break;
                    case 2: cells_stack.Push(2); res[row, column].up = false; row++; res[row, column].down = false; break;
                    case 3: cells_stack.Push(3); res[row, column].left = false; column--; res[row, column].right = false; break;
                    case 4: cells_stack.Push(4); res[row, column].right = false; column++; res[row, column].left = false; break;
                }
            }
        }

        public static Cell[,] Solve_Labyrinth(Cell[,] labyrinth, Random random, ref int[] start, ref int[] end, bool unFixedDots = true)
        {
            int n = labyrinth.GetLength(0);
            Cell[,] res = new Cell[n, n];
            Stack<int> cells_stack = new Stack<int>();


            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                {
                    res[i, j] = labyrinth[i, j];
                    res[i, j].visited = false;
                }
            if (unFixedDots)
            {
                start = Get_Start_Cell(n, random);
                end = Get_Start_Cell(n, random);
            }

            int row = start[0];
            int column = start[1];

            res[start[0], start[1]].cellColor = 3;


            int new_direction = 1;

            while (true)
            {
                if ((row == end[0]) && (column == end[1]))
                {
                    res[row, column].cellColor = 4;
                    return res;
                }
                res[row, column].visited = true;

                new_direction = Get_Direction(res, row, column, random, false);

                if (new_direction == 0)
                {
                    while (new_direction == 0)
                    {
                        res[row, column].cellColor = 2;
                        int backward = cells_stack.Pop();
                        res[row, column].direction = backward;
                        switch (backward)
                        {
                            case 1: row++; break;
                            case 2: row--; break;
                            case 3: column++; break;
                            case 4: column--; break;
                        }
                        new_direction = Get_Direction(res, row, column, random, false);
                    }
                }
                res[row, column].direction = new_direction;
                switch (new_direction)
                {
                    case 1: cells_stack.Push(1); row--; break;
                    case 2: cells_stack.Push(2); row++; break;
                    case 3: cells_stack.Push(3); column--; break;
                    case 4: cells_stack.Push(4); column++; break;
                }
                res[row, column].cellColor = 1;
            }
        }
    }

}

