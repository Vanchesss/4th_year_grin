﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FunctionPlotter = FireGrid.FunctionPlotter;

namespace Pendulum
{
    public partial class Form1 : Form
    {
        FunctionPlotter animator;
        double x_coord = 5;
        double y_coord = 5;
        int n = 1;

        public Form1()
        {
            InitializeComponent();
        }

        private void Timer_Animation_Tick(object sender, EventArgs e)
        {
            numUD_Test.Value = n;
            n++;
            Draw_Font();
        }

        private void Draw_Font()
        {
            x_coord = Pendulum.harm(2, 0, 2, (double)n/10);
            y_coord = Math.Abs(Pendulum.harm(2, 0, 2, (double)n / 10));
            animator = new FunctionPlotter(pb_Pendulum, 0, 10, 0, 10);
            animator.SetGrades(10, 10);
            animator.Clear();
            animator.XGrid();
            animator.YGrid();
            animator.DrawPendulum(x_coord+5, y_coord+5, 5, 9);
        }
        private void But_start_Click(object sender, EventArgs e)
        {
            n = Convert.ToInt16(numUD_Test.Value);
            animator = new FunctionPlotter(pb_Pendulum, 0, 10, 0, 10);
            animator.SetGrades(10, 10);
            animator.XGrid();
            animator.YGrid();
            animator.DrawEllipse(5, 5, 1, 1);
            timer_Animation.Start();
        }
    }
}
