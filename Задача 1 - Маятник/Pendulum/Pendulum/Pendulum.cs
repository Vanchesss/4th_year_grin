﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Pendulum
{
    class Pendulum
    {
        public static double Harm(double A, double Phi, double Fri, double t)
        {
            return A * Math.Sin(Fri * t + Phi);
        }

        static double Diffur(double t, double q, double fri, double l, double A)
        {
            return (A * Math.Pow(fri, 2) * Math.Sin(fri * t) * Math.Cos(q) - 9.8 * Math.Sin(q)) / l;
        }

        public static void RungeKutta(double t, ref double q, ref double vq, double fri, double l, double A, double tau)
        {
            double k1, k2, k3, k4;

            k1 = Diffur(t, q, fri, l, A) * tau;
            k2 = Diffur(t + tau / 2, q + vq * k1 / 2, fri, l, A) * tau;
            k3 = Diffur(t + tau / 2, q + vq * k2 / 2, fri, l, A) * tau;
            k4 = Diffur(t + tau, q + vq * k3, fri, l, A) *tau;


        }


    }
}
