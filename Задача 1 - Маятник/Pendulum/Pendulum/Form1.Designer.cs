﻿namespace Pendulum
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pb_Pendulum = new System.Windows.Forms.PictureBox();
            this.timer_Animation = new System.Windows.Forms.Timer(this.components);
            this.but_start = new System.Windows.Forms.Button();
            this.numUD_Test = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Pendulum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_Test)).BeginInit();
            this.SuspendLayout();
            // 
            // pb_Pendulum
            // 
            this.pb_Pendulum.Location = new System.Drawing.Point(187, 12);
            this.pb_Pendulum.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pb_Pendulum.Name = "pb_Pendulum";
            this.pb_Pendulum.Size = new System.Drawing.Size(667, 615);
            this.pb_Pendulum.TabIndex = 0;
            this.pb_Pendulum.TabStop = false;
            // 
            // timer_Animation
            // 
            this.timer_Animation.Tick += new System.EventHandler(this.Timer_Animation_Tick);
            // 
            // but_start
            // 
            this.but_start.Location = new System.Drawing.Point(51, 89);
            this.but_start.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.but_start.Name = "but_start";
            this.but_start.Size = new System.Drawing.Size(75, 23);
            this.but_start.TabIndex = 1;
            this.but_start.Text = "Старт";
            this.but_start.UseVisualStyleBackColor = true;
            this.but_start.Click += new System.EventHandler(this.But_start_Click);
            // 
            // numUD_Test
            // 
            this.numUD_Test.Location = new System.Drawing.Point(29, 130);
            this.numUD_Test.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numUD_Test.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numUD_Test.Name = "numUD_Test";
            this.numUD_Test.Size = new System.Drawing.Size(120, 22);
            this.numUD_Test.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 674);
            this.Controls.Add(this.numUD_Test);
            this.Controls.Add(this.but_start);
            this.Controls.Add(this.pb_Pendulum);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pb_Pendulum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_Test)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_Pendulum;
        private System.Windows.Forms.Timer timer_Animation;
        private System.Windows.Forms.Button but_start;
        private System.Windows.Forms.NumericUpDown numUD_Test;
    }
}

