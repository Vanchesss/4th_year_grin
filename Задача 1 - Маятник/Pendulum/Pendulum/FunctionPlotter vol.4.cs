﻿/******************************************************************************************************************************************************
* File:	FunctionPlotter.cs															                                                                  *
* Author: Alexander Kreuz																			                                                  *
* Version: v2.5	BETA																					                                              *
* Date:	29.09.2018																					                                                  *
******************************************************************************************************************************************************/

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Collections.Generic;


namespace FireGrid
{
    public sealed class FunctionPlotter
    {
        private double WXmin, WXmax, WYmin, WYmax;
        private int nx, ny; // grid lines
        private double mx, my; // scaling coefficients
        private double dx, dy; // 1px ~ dx real world coordinates
        private Graphics graphics;
        private PictureBox plot;
        private Bitmap bitmap;
        public static Random random = new Random();


        #region ENUMS
        /// <summary>
        /// Position enumeration for determining legend and text positions.
        /// </summary>
        public enum Position
        {
            TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT, TOP_CENTER, BOTTOM_CENTER, RIGHT_CENTER
        }
        #endregion

        #region CONSTRUCTORS
        /// <summary>
        /// Constructor for a new Function Plotter.
        /// </summary>
        /// <param name="pictureBox">A <see cref="PictureBox"/> used for getting a graphics object and contain graphs.</param>
        /// <param name="x1">Minimum X-Axis value.</param>
        /// <param name="x2">Maximum X-Axis value.</param>
        /// <param name="y1">Minimum Y-Axis value.</param>
        /// <param name="y2">Maximum Y-Axis value.</param>
        public FunctionPlotter(PictureBox pictureBox, double x1, double x2, double y1, double y2, bool bmp = true)
        {
            if (bmp)
            {
                WXmin = x1; WXmax = x2;
                WYmin = y1; WYmax = y2;
                plot = pictureBox;
                SetGrades(20, 20);
                dx = (WXmax - WXmin) / nx;
                dy = (WYmax - WYmin) / ny;
                mx = (plot.Width) / (WXmax - WXmin);
                my = (plot.Height) / (WYmax - WYmin);
                bitmap = new Bitmap(plot.Width, plot.Height);
                //graphics = Graphics.FromHwnd(plot.Handle);
                graphics = Graphics.FromImage(bitmap);
                plot.Image = bitmap;
            }
            else
            {
                WXmin = x1; WXmax = x2;
                WYmin = y1; WYmax = y2;
                plot = pictureBox;
                SetGrades(20, 20);
                dx = (WXmax - WXmin) / nx;
                dy = (WYmax - WYmin) / ny;
                mx = (plot.Width) / (WXmax - WXmin);
                my = (plot.Height) / (WYmax - WYmin);
                graphics = Graphics.FromHwnd(plot.Handle);

            }
        }
        #endregion

        /// <summary>
        /// Sets the <see cref="SmoothingMode"/> of this function plotter.
        /// </summary>
        /// <param name="mode">The smoothing mode to be used to drawing and plotting.</param>
        public void SetSmoothingMode(SmoothingMode mode)
        {
            graphics.SmoothingMode = mode;
        }

        /// <summary>
        /// Sets a new interval for the X-Axis.
        /// </summary>
        /// <param name="x1">New minimum X-Axis value.</param>
        /// <param name="x2">New maximum X-Axis value.</param>
        public void MinMaxX(double x1, double x2)
        {
            WXmin = x1; WXmax = x2;
            dx = (WXmax - WXmin) / nx;
        }

        /// <summary>
        /// Sets a new interval for the Y-Axis.
        /// </summary>
        /// <param name="y1">New minimum Y-Axis value.</param>
        /// <param name="y2">New naximum Y-Axis value.</param>
        public void MinMaxY(double y1, double y2)
        {
            WYmin = y1; WYmax = y2;
            dy = (WYmax - WYmin) / ny;
        }

        /// <summary>
        /// Clears the coordinate plain and sets its background color to <see cref="Color.White"./>.
        /// </summary>
        public void Clear()
        {
            graphics.Clear(Color.White);
        }

        /// <summary>
        /// Clears the coordinate plain and sets a new background <see cref="Color"/.>
        /// </summary>
        /// <param name="color">The color to be used as new background color.</param>
        public void Clear(Color color)
        {
            graphics.Clear(color);
        }

        public void RestoreImage()
        {
            graphics.DrawImage(bitmap, 0, 0);
        }

        /// <summary>
        /// Configures the grid alignment.
        /// </summary>
        /// <param name="inx">The amount of cells along the X-Axis.</param>
        /// <param name="iny">The amount of cells along the Y-Axis.</param>
        public void SetGrades(int inx, int iny)
        {
            nx = inx; ny = iny;
            dx = (WXmax - WXmin) / nx;
            dy = (WYmax - WYmin) / ny;
        }

        /// <summary>
        /// Draws the grid along the X direction.
        /// </summary>
        public void XGrid()
        {
            for (int i = 1; i < nx; i++)
                VerticalLine(i);
        }

        /// <summary>
        /// Draws the grid along the Y direction.
        /// </summary>
        public void YGrid()
        {
            for (int i = 1; i < ny; i++)
                HorizontalLine(i);
        }

        /// <summary>
        /// Draws an X-Axis with a given axis label.
        /// </summary>
        /// <param name="text">The axis label to be used for this axis. Will be <code>"X"</code> by default.</param>
        public void AxisX(bool arrow = false, String text = "X")
        {
            Pen pen = new Pen(Color.Black);
            pen.EndCap = arrow ? LineCap.ArrowAnchor : LineCap.Flat;
            graphics.DrawLine(pen, OutX(WXmin), OutY(0), OutX(WXmax) - (arrow ? 3 : 0), OutY(0));
            graphics.DrawString(text, SystemFonts.DefaultFont, Brushes.Black, OutX(WXmax) - text.Length * 17, OutY(0));

        }

        /// <summary>
        /// Draws a Y-Axis with a given axis label.
        /// </summary>
        /// <param name="text">The axis label to be used for this axis. Will be <code>"Y"</code> by default.</param>
        public void AxisY(bool arrow = false, String text = "Y")
        {
            Pen pen = new Pen(Color.Black);
            pen.EndCap = arrow ? LineCap.ArrowAnchor : LineCap.Flat;
            graphics.DrawLine(pen, OutX(0), OutY(WYmin), OutX(0), OutY(WYmax));
            graphics.DrawString(text, SystemFonts.DefaultFont, Brushes.Black, OutX(0) + 5, OutY(WYmax));
        }

        /// <summary>
        /// Draws a Y-Axis with a given axis label.
        /// </summary>
        /// <param name="text">The axis label to be used for this axis. Will be <code>"Y"</code> by default.</param>
        public void AxisY(double xPos, bool arrow = false, String text = "Y")
        {
            Pen pen = new Pen(Color.Black);
            pen.EndCap = arrow ? LineCap.ArrowAnchor : LineCap.Flat;
            graphics.DrawLine(pen, OutX(xPos), OutY(WYmin), OutX(xPos), OutY(WYmax));
            graphics.DrawString(text, SystemFonts.DefaultFont, Brushes.Black, OutX(xPos) + 5, OutY(WYmax));
        }

        /// <summary>
        /// Labels the X-Axis. Label frequency depends on the grid configuration.
        /// </summary>
        public void AxisXLabel(string FormatString = "{0:f3}", bool startFromZero = false, int step = 1)
        {
            Font font = new Font(FontFamily.GenericMonospace, 9.0f, FontStyle.Regular, GraphicsUnit.Pixel);
            for (int i = startFromZero ? 2 : 1; i < nx; i += step)
            {
                string caption = String.Format(FormatString, WXmin + i * dx);
                if (WXmin + i * dx != 0)
                    graphics.DrawString(caption, font, Brushes.Black, OutX(WXmin + i * dx) - font.SizeInPoints * caption.Length / 2, OutY(0) + font.SizeInPoints / 2);
            }
        }





        /// <summary>
        /// Labels the X-Axis. Label frequency depends on the grid configuration.
        /// </summary>
        public void AxisXLabel(int startValue, int step = 1, string formatString = "{0:f3}")
        {
            Font font = new Font(FontFamily.GenericMonospace, 9.0f, FontStyle.Regular, GraphicsUnit.Pixel);
            for (int i = startValue; i < nx; i += step)
            {
                string caption = String.Format(formatString, WXmin + i * dx);
                if (WXmin + i * dx != 0)
                    graphics.DrawString(caption, font, Brushes.Black, OutX(WXmin + i * dx) - font.SizeInPoints * caption.Length / 2, OutY(0) + font.SizeInPoints / 2);
            }
        }

        /// <summary>
        /// Labels the Y-Axis. Label frequency depends on the grid configuration.
        /// </summary>
        public void AxisYLabel(string formatString = "{0:f2}", double xPos = 0.0)
        {
            Font font = new Font(FontFamily.GenericMonospace, 9.0f, GraphicsUnit.Pixel);
            for (int i = 1; i < ny; i++)
            {
                string caption = String.Format(formatString, WYmin + i * dy);
                if (WYmin + i * dy != 0)
                    graphics.DrawString(caption, font, Brushes.Black, OutX(xPos) - font.SizeInPoints * caption.Length, OutY(WYmin + i * dy) - font.SizeInPoints);
            }
        }

        /// <summary>
        /// Draws the k-th horizontal grid line.
        /// </summary>
        /// <param name="k">Horizontal grid line index.</param>
        private void HorizontalLine(int k)
        {
            Pen pen = new Pen(Color.Gray)
            {
                DashStyle = DashStyle.Dash
            };
            graphics.DrawLine(pen, OutX(WXmin), OutY(WYmin + k * dy), OutX(WXmax), OutY(WYmin + k * dy));
        }

        /// <summary>
        /// Draws the k-th vertical grid line.
        /// </summary>
        /// <param name="k">Vertical grid line index.</param>
        private void VerticalLine(int k)
        {
            Pen pen = new Pen(Color.Gray)
            {
                DashStyle = DashStyle.Dash
            };
            graphics.DrawLine(pen, OutX((WXmin + k * dx)), OutY(WYmin), OutX((WXmin + k * dx)), OutY(WYmax));
        }

        /// <summary>
        /// Draws a horizontal line with the given parameters.
        /// </summary>
        /// <param name="y">Height in world coordinates.</param>
        /// <param name="color">Line color.</param>
        /// <param name="dashStyle"><see cref="DashStyle"/> for the horizontal line, <see cref="DashStyle.Solid"/> by default.</param>
        public void HorizontalLine(double y, Color color, DashStyle dashStyle = DashStyle.Solid)
        {
            Pen pen = new Pen(color)
            {
                DashStyle = dashStyle
            };
            graphics.DrawLine(pen, 0, OutY(y), plot.Width, OutY(y));
        }

        /// <summary>
        /// Draws a vertical line with the given parameters.
        /// </summary>
        /// <param name="x">Position in world coordinates.</param>
        /// <param name="color">Line color.</param>
        /// <param name="dashStyle"><see cref="DashStyle"/> for the vertical line, <see cref="DashStyle.Solid"/> by default.</param>
        public void VerticalLine(double x, Color color, DashStyle dashStyle = DashStyle.Solid)
        {
            Pen pen = new Pen(color)
            {
                DashStyle = dashStyle
            };
            graphics.DrawLine(pen, OutX(x), 0, OutX(x), plot.Height);
        }

        /// <summary>
        /// Draws text at the given position.
        /// </summary>
        /// <param name="x">X-position of the text in world coordinates.</param>
        /// <param name="y">Y-position of the text in world coordinates.</param>
        /// <param name="text">The string to be drawn.</param>
        /// <param name="fontSize">Optional font size parameter. 12px by default.</param>
        public void DrawString(double x, double y, String text, float fontSize = 12.0f)
        {
            Font font = new Font(FontFamily.GenericMonospace, fontSize, GraphicsUnit.Pixel);
            graphics.DrawString(text, font, Brushes.Black, OutX(x), OutY(y));
        }

        /// <summary>
        /// Draws text at the given position.
        /// </summary>
        /// <param name="x">X-position of the text in world coordinates.</param>
        /// <param name="y">Y-position of the text in world coordinates.</param>
        /// <param name="text">The string to be drawn.</param>
        /// <param name="color">A <see cref="Brush"/> for custom colors. Can be chosen from <see cref="Brushes"/>.</param>
        /// <param name="fontSize">Optional font size parameter. 12px by default.</param>
        public void DrawString(double x, double y, String text, Brush color, float fontSize = 12.0f)
        {
            Font font = new Font(FontFamily.GenericMonospace, fontSize, GraphicsUnit.Pixel);
            graphics.DrawString(text, font, color, OutX(x), OutY(y));
        }

        /// <summary>
        /// Draws text at the given <see cref="Position"/>.
        /// </summary>
        /// <param name="text">The string to be drawn.</param>
        /// <param name="pos">The <see cref="Position"/> of the string.</param>
        /// <param name="brush">A <see cref="Brush"/> for custom colors. Can be chosen from <see cref="Brushes"/>.</param>
        /// <param name="style">A <see cref="FontStyle"/> for underlining and other styles. <see cref="FontStyle.Regular"/> by default.</param>
        /// <param name="fontSize">Optional font size parameter. 12px by default.</param>
        public void DrawString(string text, Position pos, Brush brush, FontStyle style = FontStyle.Regular, float fontSize = 12.0f)
        {
            Font font = new Font(FontFamily.GenericMonospace, fontSize, style, GraphicsUnit.Pixel);
            switch (pos)
            {
                case Position.TOP_LEFT:
                    graphics.DrawString(text, font, brush, font.SizeInPoints, font.SizeInPoints);
                    break;
                case Position.TOP_RIGHT:
                    graphics.DrawString(text, font, brush, plot.Width - text.Length * font.SizeInPoints, font.SizeInPoints);
                    break;
                case Position.BOTTOM_LEFT:
                    graphics.DrawString(text, font, brush, font.SizeInPoints, plot.Height - (font.Height + font.SizeInPoints));
                    break;
                case Position.BOTTOM_RIGHT:
                    graphics.DrawString(text, font, brush, plot.Width - text.Length * font.SizeInPoints, plot.Height - (font.Height + font.SizeInPoints));
                    break;
                case Position.TOP_CENTER:
                    graphics.DrawString(text, font, brush, plot.Width / 2 - text.Length / 2 * font.SizeInPoints, font.SizeInPoints);
                    break;
                case Position.BOTTOM_CENTER:
                    graphics.DrawString(text, font, brush, plot.Width / 2 - text.Length / 2 * font.SizeInPoints, plot.Height - (font.Height + font.SizeInPoints));
                    break;
            }
        }

        /// <summary>
        /// Draws text at the given <see cref="Position"/>. The string will be black. Use different overload function for custom colors.
        /// </summary>
        /// <param name="text">The string to be drawn.</param>
        /// <param name="pos">The <see cref="Position"/> of the string.</param>
        /// <param name="style">A <see cref="FontStyle"/> for underlining and other styles. <see cref="FontStyle.Regular"/> by default.</param>
        /// <param name="fontSize">Optional font size parameter. 12px by default.</param>
        public void DrawString(string text, Position pos, FontStyle style = FontStyle.Regular, float fontSize = 12.0f)
        {
            DrawString(text, pos, Brushes.Black, style, fontSize);
        }

        /// <summary>
        /// Draws text at the given <see cref="Position"/>. The string will be black. Use different overload function for custom colors
        /// and font style configurations.
        /// </summary>
        /// <param name="text">The string to be drawn.</param>
        /// <param name="pos">The <see cref="Position"/> of the string.</param>
        /// <param name="fontSize">Optional font size parameter. 12px by default.</param>
        public void DrawString(string text, Position pos, float fontSize = 12.0f)
        {
            DrawString(text, pos, Brushes.Black, FontStyle.Regular, fontSize);
        }

        /// <summary>
        /// Draws text at the given <see cref="Position"/>. The string will be black. Use different overload function for custom font styles.
        /// </summary>
        /// <param name="text">The string to be drawn.</param>
        /// <param name="pos">The <see cref="Position"/> of the string.</param>
        /// <param name="brush">A brush for setting a custom color.</param>
        /// <param name="fontSize">Optional font size parameter. 12px by default.</param>
        public void DrawString(string text, Position pos, Brush brush, float fontSize = 12.0f)
        {
            DrawString(text, pos, brush, FontStyle.Regular, fontSize);
        }

        /// <summary>
        /// Auxiliary function for finding the longest <see cref="string"/> from a sequence of strings.
        /// </summary>
        /// <param name="labels">The sequence for which the longest element shall be found.</param>
        /// <returns>The longest <see cref="string"/> from <paramref name="labels"/>.</returns>
        private string LongestLabel(string[] labels)
        {
            string longest = "";
            foreach (string str in labels)
                if (str.Length > longest.Length)
                    longest = str;
            return longest;
        }

        /// <summary>
        /// Auxiliary function. Converts an array of <see cref="object"/>s to an array of <see cref="string"/>s
        /// by invocing the <see cref="Object.ToString"/> method for each element.
        /// </summary>
        /// <param name="objects">The objects to be converted to strings.</param>
        /// <returns>An array of string representations of the <paramref name="objects"/>.</returns>
        private string[] ToString(object[] objects)
        {
            string[] strings = new string[objects.Length];
            for (int i = 0; i < objects.Length; i++)
                strings[i] = objects[i].ToString();
            return strings;
        }

        /// <summary>
        /// Creates a legend with the given entries which can be used to explain the graphics plotted.
        /// A legend can be modified with different parameters for setting its style and size.
        /// </summary>
        /// <param name="labels">An array of labels to be used for the legend's entries.</param>
        /// <param name="colors">An array of colors to be used for the legend's entries.</param>
        /// <param name="pos"><see cref="Position"/> of the legend.</param>
        /// <param name="fontSize">Font size for legend labels.</param>
        /// <param name="opacity">Defines the opacity of the legend background (0 - completely transparent, 255 - completely non-transparent).</param>
        /// <param name="useDarkTheme">This is beta functionality. Whether the dark theme should be used or not.</param>
        public void DrawLegend(string[] labels, Color[] colors, Position pos, float fontSize = 10.0f, byte opacity = 150, bool useDarkTheme = false)
        {
            Brush fontColor = useDarkTheme ? Brushes.White : Brushes.Black;
            Brush backgroundColor = new SolidBrush(useDarkTheme ? Color.FromArgb(150, 0, 0, 0) : Color.FromArgb(150, 255, 255, 255));
            Font font = new Font(FontFamily.GenericMonospace, fontSize, FontStyle.Italic, GraphicsUnit.Pixel);
            Rectangle frame = new Rectangle(0, 0, (int)graphics.MeasureString("■ " + LongestLabel(labels), font).Width, labels.Length * font.Height + 5);
            switch (pos)
            {
                case Position.TOP_LEFT:
                    frame.X = 5;
                    frame.Y = 5;
                    break;
                case Position.TOP_RIGHT:
                    frame.X = plot.Width - frame.Width - 8;
                    frame.Y = 5;
                    break;
                case Position.BOTTOM_LEFT:
                    frame.X = 5;
                    frame.Y = plot.Height - frame.Height - 8;
                    break;
                case Position.BOTTOM_RIGHT:
                    frame.X = plot.Width - frame.Width - 8;
                    frame.Y = plot.Height - frame.Height - 8;
                    break;
                case Position.TOP_CENTER:
                    frame.X = plot.Width / 2 - frame.Width / 2;
                    frame.Y = 5;
                    break;
                case Position.BOTTOM_CENTER:
                    frame.X = plot.Width / 2 - frame.Width / 2;
                    frame.Y = plot.Height - frame.Height - 8;
                    break;
            }
            graphics.FillRectangle(backgroundColor, frame);
            graphics.DrawRectangle(Pens.Black, frame);
            for (int i = 0; i < labels.Length; i++)
            {
                graphics.DrawString(colors[i] == Color.White ? "□ " : "■ "/*"●"*/, font, new SolidBrush(colors[i] == Color.White ? Color.Black : colors[i]), frame.X + 2, frame.Y + 2 + i * font.Height);
                graphics.DrawString(labels[i], font, fontColor, frame.X + 2 + font.SizeInPoints, frame.Y + 2 + i * font.Height);
            }
        }

        /// <summary>
        /// Creates an infobox with the given entries which can be used to document the parameter values used for creating the graph.
        /// An infobox can be modified with different parameters for setting its size and position.
        /// </summary>
        /// <param name="parameters">An array of parameter names to be used for the infobox entries.</param>
        /// <param name="colors">An array of values to be used for the infobox's entries.</param>
        /// <param name="pos"><see cref="Position"/> of the infobox.</param>
        /// <param name="fontSize">Font size for infobox entries, 10px by default.</param>
        /// <param name="opacity">Defines the opacity of the legend background (0 - completely transparent, 255 - completely non-transparent).</param>
        public void DrawParameterInformation(string[] parameters, object[] values, Position pos, float fontSize = 10.0f, byte opacity = 150)
        {
            Brush fontColor = Brushes.Black;
            Brush backgroundColor = new SolidBrush(Color.FromArgb(opacity, 255, 255, 255));
            Font font = new Font(FontFamily.GenericMonospace, fontSize, FontStyle.Italic, GraphicsUnit.Pixel);
            string[] stringValues = ToString(values);
            for (int i = 0; i < values.Length; i++)
                stringValues[i] = parameters[i] + ": " + stringValues[i];
            //Rectangle frame = new Rectangle(0, 0, (int)graphics.MeasureString(LongestLabel(parameters) + ": " + LongestLabel(stringValues), font).Width + 5, parameters.Length * font.Height + 5);
            Rectangle frame = new Rectangle(0, 0, (int)graphics.MeasureString(LongestLabel(stringValues), font).Width + 5, parameters.Length * font.Height + 5);
            switch (pos)
            {
                case Position.TOP_LEFT:
                    frame.X = 5;
                    frame.Y = 5;
                    break;
                case Position.TOP_RIGHT:
                    frame.X = plot.Width - frame.Width - 8;
                    frame.Y = 5;
                    break;
                case Position.BOTTOM_LEFT:
                    frame.X = 5;
                    frame.Y = plot.Height - frame.Height - 8;
                    break;
                case Position.BOTTOM_RIGHT:
                    frame.X = plot.Width - frame.Width - 8;
                    frame.Y = plot.Height - frame.Height - 8;
                    break;
                case Position.TOP_CENTER:
                    frame.X = plot.Width / 2 - frame.Width / 2;
                    frame.Y = 5;
                    break;
                case Position.BOTTOM_CENTER:
                    frame.X = plot.Width / 2 - frame.Width / 2;
                    frame.Y = plot.Height - frame.Height - 8;
                    break;
                case Position.RIGHT_CENTER:
                    frame.X = plot.Width - frame.Width - 8;
                    frame.Y = plot.Height / 2 - frame.Height - 8;
                    break;
            }
            graphics.FillRectangle(backgroundColor, frame);
            graphics.DrawRectangle(Pens.Black, frame);
            for (int i = 0; i < stringValues.Length; i++)
                //graphics.DrawString(parameters[i] + ": " + stringValues[i], font, fontColor, frame.X + 2, frame.Y + 2 + i * font.Height);
                graphics.DrawString(stringValues[i], font, fontColor, frame.X + 2, frame.Y + 2 + i * font.Height);
        }

        /// <summary>
        /// Creates a textbox. The title, if it exists, will be unterlined to the full width of the box.
        /// Multiline text can be entered by using the escape sequence <code>\n</code> to separate lines in the entered string.
        /// Multiline titles are not supported and may lead to unpredictable behaviour.
        /// Providing title and text is optional, so you can create either plain textboxes without a headline or a headline, for example
        /// for documenting your graph and give it a title for usage in documentations and reports.
        /// </summary>
        /// <param name="title">The textbox headline. This must be a single line <see cref="string"/> or <code>null</code>.</param>
        /// <param name="text">The textbox content. This can be a multiline string separated by \n escape sequences or null.</param>
        /// <param name="pos">The <see cref="Position"/> of the textbox.</param>
        /// <param name="color">The <see cref="Color"/> in which title and text will be displayed.</param>
        /// <param name="fontSize">The font size to be used for title and text, 10px by default.</param>
        public void DrawTextBox(string title, string text, Position pos, Color color, float fontSize = 10.0f, byte opacity = 150)
        {
            Brush fontColor = new SolidBrush(color);
            Brush backgroundColor = new SolidBrush(Color.FromArgb(opacity, 255, 255, 255));
            Font font = new Font(FontFamily.GenericMonospace, fontSize, FontStyle.Italic, GraphicsUnit.Pixel);
            Font headlineFont = new Font(font, FontStyle.Underline);
#pragma warning disable IDE0031
            string[] textAsLines = text == null ? null : text.Split('\n');
#pragma warning restore IDE0031
            Rectangle frame = new Rectangle(0, 0, (int)Math.Max(text == null ? 0 : graphics.MeasureString(LongestLabel(textAsLines), font).Width, (title == null ? 0 : graphics.MeasureString(title, font).Width)) + 5, (text == null ? 0 : textAsLines.Length * font.Height) + (title == null ? 0 : font.Height) + 5);
            switch (pos)
            {
                case Position.TOP_LEFT:
                    frame.X = 5;
                    frame.Y = 5;
                    break;
                case Position.TOP_RIGHT:
                    frame.X = plot.Width - frame.Width - 8;
                    frame.Y = 5;
                    break;
                case Position.BOTTOM_LEFT:
                    frame.X = 5;
                    frame.Y = plot.Height - frame.Height - 8;
                    break;
                case Position.BOTTOM_RIGHT:
                    frame.X = plot.Width - frame.Width - 8;
                    frame.Y = plot.Height - frame.Height - 8;
                    break;
                case Position.TOP_CENTER:
                    frame.X = plot.Width / 2 - frame.Width / 2;
                    frame.Y = 5;
                    break;
                case Position.BOTTOM_CENTER:
                    frame.X = plot.Width / 2 - frame.Width / 2;
                    frame.Y = plot.Height - frame.Height - 8;
                    break;
            }
            graphics.FillRectangle(backgroundColor, frame);
            graphics.DrawRectangle(Pens.Black, frame);
            if (title != null)
                graphics.DrawString(text == null ? title : title.PadRight(LongestLabel(textAsLines).Length - 1) + (char)127, headlineFont, fontColor, frame.X + 2, frame.Y + 2);
            if (textAsLines != null)
                for (int i = 0; i < textAsLines.Length; i++)
                    graphics.DrawString(textAsLines[i], font, fontColor, frame.X + 2, frame.Y + 2 + (i + (title == null ? 0 : 1)) * font.Height);
        }

        /// <summary>
        /// Draws an ellipse with the given parameters.
        /// </summary>
        /// <param name="x">X-position of the ellipse in world coordinates.</param>
        /// <param name="y">Y-position of the ellipse in world coordinates.</param>
        /// <param name="width">Width of the ellipse in world coordinates.</param>
        /// <param name="height">Height of the ellipse in world coordinates.</param>
        public void DrawEllipse(double x, double y, double width, double height)
        {
            Pen pen = new Pen(Color.Black);
            graphics.DrawEllipse(pen, OutX(x) - OutX(width) / 2, OutY(y) - OutY(height,true) / 2, OutX(width), OutY(height,true));
        }

        public void DrawColoredRectangle(double x, double y, double width, double height, Color color)
        {
            Pen pen = new Pen(color);
            SolidBrush brush = new SolidBrush(color);
            //graphics.DrawRectangle(pen, OutX(x) - OutX(width) / 2, OutY(y) - OutY(height,true) / 2, OutX(width), OutY(height,true));
            graphics.FillRectangle(brush, OutX(x) - OutX(width) / 2, OutY(y) - OutY(height, true) / 2, OutX(width) + 1, OutY(height, true) + 1);
        }

        /// <summary>
        /// Mark a point at the coordinate plains.
        /// </summary>
        /// <param name="x">X-position of the point in world coordinates.</param>
        /// <param name="y">Y-position of the point in world coordinates.</param>
        public void DrawPoint(double x, double y)
        {
            Brush brush = Brushes.Black;
            graphics.FillEllipse(brush, OutX(x) - 3, OutY(y) - 3, 6, 6);
        }


        public void DrawWhitePoint(double x, double y)
        {

            Brush brush = Brushes.Black;
            Pen pen = new Pen(brush, -1);
            graphics.DrawEllipse(pen, OutX(x) - 3, OutY(y) - 3, 6, 6);
        }
        /// <summary>
        /// Mark a labelled point at the coordinate plains.
        /// </summary>
        /// <param name="x">X-position of the point in world coordinates.</param>
        /// <param name="y">Y-position of the point in world coordinates.</param>
        public void DrawLabelledPoint(double x, double y, string label)
        {
            Brush brush = Brushes.Black;
            graphics.FillEllipse(brush, OutX(x) - 3, OutY(y) - 3, 6, 6);
            Font font = new Font(FontFamily.GenericMonospace, 10.0f, FontStyle.Italic, GraphicsUnit.Pixel);
            graphics.DrawString(label, font, Brushes.Black, OutX(x) + 10, OutY(y) - 10);
        }

        /// <summary>
        /// Draws a straight line between two given points.
        /// </summary>
        /// <param name="x1">X-position of the start point in world coordinates.</param>
        /// <param name="y1">Y-position of the start point in world coordinates.</param>
        /// <param name="x2">X-position of the end point in world coordinates.</param>
        /// <param name="y2">Y-position of the end point in world coordinates.</param>
        /// <param name="color">Line color.</param>
        /// <param name="dashStyle">Optional <see cref="DashStyle"/> parameter. Will be <see cref="DashStyle.Solid"/> by default.</param>
        public void DrawLine(double x1, double y1, double x2, double y2, Color color, DashStyle dashStyle = DashStyle.Solid)
        {
            Pen pen = new Pen(color);
            pen.DashStyle = dashStyle;
            graphics.DrawLine(pen, OutX(x1), OutY(y1), OutX(x2), OutY(y2));
        }

        /// <summary>
        /// Fills an ellipse with the given parameters.
        /// </summary>
        /// <param name="brush">The brush to be used for drawing. You can use <see cref="Brushes"/> as well as custom brushes.
        /// This is to allow for custom color selection as color constants are unknown at compilation time.</param>
        /// <param name="x">X-position of the ellipse in world coordinates.</param>
        /// <param name="y">Y-position of the ellipse in world coordinates.</param>
        /// <param name="width">Width of the ellipse.</param>
        /// <param name="height">Height of the ellipse.</param>
        public void FillEllipse(Brush brush, double x, double y, double width, double height)
        {
            graphics.FillEllipse(brush, OutX(x) - OutX(width) / 2, OutY(y) - OutY(height) / 2, OutX(width), OutY(height));
        }

        /// <summary>
        /// Plots a list of discrete floating point values as a graph (that is points will be connected).
        /// </summary>
        /// <param name="list">An array of discrete double precision floating point numbers.</param>
        /// <param name="color">The color of the resulting line.</param>
        public void ListLinePlot(double[] list, Color color, bool showPoints = false)
        {
            for (int i = 1; i < list.Length; i++)
            {
                this.DrawLine(i - 1, list[i - 1], i, list[i], color);
                if (showPoints)
                    DrawPoint(i - 1, list[i - 1]);
            }
            if (showPoints)
                DrawPoint(list.Length - 1, list[list.Length - 1]);
        }

        public void ListLinePlot(int[] list, Color color)
        {
            for (int i = 1; i < list.Length; i++)
                this.DrawLine(i - 1, list[i - 1], i, list[i], color);
        }

        /// <summary>
        /// Plots a list of discrete floating point values as a graph (that is points will be connected).
        /// </summary>
        /// <param name="list">An array of discrete double precision floating point numbers.</param>
        /// <param name="color">The color of the resulting line.</param>
        public void ListLinePlot(double[] list, Color color, int offset, bool showPoints = false)
        {
            for (int i = 1; i < list.Length; i++)
            {
                this.DrawLine(i - 1 + offset, list[i - 1], i + offset, list[i], color);
                if (showPoints)
                    DrawPoint(i - 1 + offset, list[i - 1]);
            }
            if (showPoints)
                DrawPoint(list.Length - 1 + offset, list[list.Length - 1]);
        }

        /// <summary>
        /// Plots a list of discrete floating point values as a graph (that is points will be connected).
        /// </summary>
        /// <param name="list">An array of discrete double precision floating point numbers.</param>
        /// <param name="interval">The interval width for the X-Axis in case it differs from the array indeces.</param>
        /// <param name="color">The color of the resulting line.</param>
        public void ListLinePlot(double[] list, double interval, Color color, bool showPoints = false)
        {
            double factor = interval / list.Length;
            for (int i = 1; i < list.Length; i++)
            {
                this.DrawLine(factor * (i - 1), list[i - 1], factor * i, list[i], color);
                if (showPoints)
                    DrawPoint((i - 1) * factor, list[i - 1]);
            }
            //DrawPoint(factor * list.Length, list[list.Length - 1]);
        }

        public void ListLinePlot(int[] list, double interval, Color color)
        {
            double factor = interval / list.Length;
            for (int i = 1; i < list.Length; i++)
                this.DrawLine(factor * (i - 1), list[i - 1], factor * i, list[i], color);
        }

        public void ListStatisticalPlot(double[] list, int width, Color color)
        {
            Pen pen = new Pen(color, width);
            for (int i = 0; i < list.Length; i += 16)
                if (list[i] > 0)
                    graphics.DrawLine(pen, OutX(i), OutY(0), OutX(i), OutY(list[i]));
                else
                    graphics.DrawLine(pen, OutX(i), OutY(0), OutX(i), OutY(list[i]));
        }

        /// <summary>
        /// Plots a given function of the type f(x) = y, where x and y are both real numbers.
        /// WARNING: Will crash if the function is undefined or infinity within the given interval.
        /// </summary>
        /// <param name="function">The function to be plotted.</param>
        /// <param name="color">The color to be used for plotting the function.</param>
        public void PlotFunction(Func<double, double> function, Color color)
        {
            double x1 = WXmin, y1 = function(x1), y2 = 0;
            for (double x2 = WXmin + dx / 10; x2 <= WXmax; x2 += dx / 10)
            {
                y2 = function(x2);
                graphics.DrawLine(new Pen(color), OutX(x1), OutY(y1), OutX(x2), OutY(y2));
                x1 = x2; y1 = y2;
            }
        }

        public void DrawPendulum(double pendX, double pendY, double dotX, double dotY)
        {
            DrawEllipse(pendX, pendY, 1, 1);
            DrawEllipse(dotX, dotY, 0.5, 0.5);
            DrawLine(pendX, pendY, dotX, dotY, Color.Black);
        }

        #region COORDINATE_TRANSFORMATIONS
        /// <summary>
        /// Converts world coordinates to window coordinates (pixels).
        /// </summary>
        /// <param name="x">X-coordinate in world coordinates.</param>
        /// <returns>The coordinate converted to window coordinates (pixels).</returns>
        public int OutX(double x)
        {
            return (int)Math.Floor((x - WXmin) * mx);
        }

        /// <summary>
        /// Converts world coordinates to window coordinates (pixels).
        /// </summary>
        /// <param name="y">Y-coordinate in world coordinates.</param>
        /// <returns>The coordinate converted to window coordinates (pixels).</returns>
        public int OutY(double y, bool size = false)
        {
            if (size)
                return (int)Math.Floor((y - WYmin) * my);
            else
                return (int)Math.Floor(plot.Height - (y - WYmin) * my);
        }

        /// <summary>
        /// Converts window coordinates (pixels) to world coordinates.
        /// </summary>
        /// <param name="x">X-coordinate in pixels.</param>
        /// <returns>The coordinate converted to world coordinates.</returns>
        public double InX(int x)
        {
            return (x / mx) + WXmin;
        }

        /// <summary>
        /// Converts window coordinates (pixels) to world coordinates.
        /// </summary>
        /// <param name="y">Y-coordinate in pixels.</param>
        /// <returns>The coordinate converted to world coordinates.</returns>
        public double InY(int y)
        {
            return -(y / my) + WYmax;
        }
        #endregion


    }
}
